import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class SpotifyService {

  constructor
    (
      private _http: HttpClient
    ) { 
      this.getNewReleases();
    }

   

  getNewReleases() {
    this._http.get('https://api.spotify.com/v1/browse/new-releases',
      {
        headers: {
          'Authorization': 'Bearer BQB7zeW0lELDLHI9c3_qXH39samCb8aQwO5QynelyN-qh6JMrKAnmvOfdMMt-c1gMhCR2kDpekv2ei3HZ8k'
        }
      })
      .subscribe(resp=>{
        console.log(resp);
      })
  }
}
